package com.example.services;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button startService;
    Button StopService;
    private Intent serviceIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i(getString(R.string.common_tag),"Main Activity OnCreate Thread Id= "+Thread.currentThread().getId());
        startService=(Button)findViewById(R.id.startService);
        StopService=(Button)findViewById(R.id.stopService);
        /**
         * Intiliaze the Service Intent.
         */
        serviceIntent=new Intent(this,ServiceDemo.class);
        startService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * Android recomends always use Explicit Intent to Start the Service.
                 */
                startService(serviceIntent);
            }
        });

        StopService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * Stop ing the service outside the service using Service Intent.
                 */
                stopService(serviceIntent);
            }
        });
    }
}
